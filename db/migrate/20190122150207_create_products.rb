class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.integer :category_id
      t.string :name
      t.string :slug, unique: true
      t.text :desc
      t.decimal :price, precision: 12, scale: 3
      t.integer :stock
      t.boolean :active
      t.timestamps
    end
  end
end
