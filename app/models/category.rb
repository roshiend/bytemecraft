class Category < ApplicationRecord
  before_save :uppercase
  has_many :products
  
  extend FriendlyId
  friendly_id :name, use: :slugged
  
  def uppercase
    name.upcase!
  end  
end
